var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number], 
    index: {
      type: '2dsphere', 
      sparse: true
    }
  }
});

bicicletaSchema.methods.toString = function() {
  return 'code: ' + this.code + ' | color: ' + this.color;
};

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
  return new this({
    code: code,
    color: color, 
    modelo: modelo,
    ubicacion: ubicacion
  });
};

bicicletaSchema.statics.allBicis = function(cb) {
  return this.find({}, cb).sort({code:1});
};

bicicletaSchema.statics.add = function(aBici, cb) {
  this.create(aBici, cb);
};

bicicletaSchema.statics.findByCode = function(code, cb) { // id is equal to code in my model
  return this.findOne({ code: code }, cb);
}

bicicletaSchema.statics.removeByCode = function (code, cb) {
  return this.deleteOne({ code: code }, cb)
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);
