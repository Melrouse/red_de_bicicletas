var express = require('express');
var router = express.Router();
var insBicicleta = require('../controllers/bicicleta');

router.get('/', insBicicleta.bicicleta_list);
router.get('/read/:code', insBicicleta.bicicleta_read);
router.get('/create', insBicicleta.bicicleta_create_get);
router.post('/create', insBicicleta.bicicleta_create_post);
router.post('/delete/:code', insBicicleta.bicicleta_delete_post);
router.get('/update/:code', insBicicleta.bicicileta_update_get);
router.post('/update/:code', insBicicleta.bicicleta_update_post);

module.exports = router;
