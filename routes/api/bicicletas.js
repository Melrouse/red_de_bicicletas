var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');
var express = require('express');
var router = express.Router();

router.get('/', bicicletaController.bicicleta_list);
router.get('/read/:code', bicicletaController.bicicleta_read);
router.post('/create', bicicletaController.bicicleta_create);
router.post('/update', bicicletaController.bicicleta_update);
router.delete('/delete/:code', bicicletaController.bicicleta_delete);

module.exports = router;
