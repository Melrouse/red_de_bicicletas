var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function() {
  beforeEach(function(done) { // Antes de ejecutar los tests
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB Connection error: '));
    db.once('open', function() {
      console.log('Estamos conectados a la base de datos');
      done();
    });
  });

  afterEach(function(done) { // Despues de ejecutar los tests
    Bicicleta.deleteMany({}, function(err, success) {
      if (err) console.log(err);
      mongoose.disconnect(err); // Esta linea es necesaria para cerrar la conexión a la base de datos tras la ejecución de cada prueba.
      console.log('Desconectados de la base de datos');
      done();
    });
  });

  describe('Bicicleta.createInstance', () => {
    it('Crea una instancia de Bicicleta', (done) => {
      var bici = Bicicleta.createInstance(1, "Verde", "Urbana", [37.886610834914016, -4.791306293646539]);
      console.log('Testeando la creación de la instancia de Bicicleta...');
      expect(bici.code).toBe(1);
      expect(bici.color).toBe("Verde");
      expect(bici.modelo).toBe("Urbana");
      expect(bici.ubicacion[0]).toEqual(37.886610834914016);
      expect(bici.ubicacion[1]).toEqual(-4.791306293646539);
      done();
    });
  });

  describe('Bicicleta.allBicis', () => {
    it('Comienza vacia', (done) => {
      Bicicleta.allBicis(function(err, bicis) {
        console.log('Testeando que la propiedad allBicis de Bicicleta este vacia al comienzo...');
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe('Bicicleta.add', () => {
    it('Agrega solo una bici', (done) => {
      var aBici = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
      Bicicleta.add(aBici, function(err, newBici) {
        if (err) console.log(err);
        Bicicleta.allBicis(function(err, bicis) {
          console.log('Testeando el metodo add de Bicicletas...');
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);
          done();
        });
      });
    });
  });

  describe('Bicicleta.findByCode', () => {
    it('Debe devolver la bicicleta con codigo 2', (done) => {
      var aBici = Bicicleta.createInstance(1, "Verde", "Urbana", [37.886610834914016, -4.791306293646539]);
      var bBici = Bicicleta.createInstance(2, "Blanca", "Urbana", [37.885186766470405, -4.7914177352839795]);
      var cBici = Bicicleta.createInstance(3, "Verde", "Urbana", [37.877431464481596, -4.8012672656286846]);

      Bicicleta.add(aBici, function(err, newBici) {
        if (err) console.log(err);
        Bicicleta.add(bBici, function(err, newBici) {
          if (err) console.log(err);
          Bicicleta.add(cBici, function(err, newBici) {
            if (err) console.log(err);
            Bicicleta.findByCode(bBici.code, function(err, bici) {
              console.log('Testeando el metodo findById de Bicicletas...');
              expect(bici.code).toEqual(bBici.code);
              expect(bici.ubicacion[0]).toEqual(37.885186766470405);
              done();
            });
          });
        });
      });
    });
  });

  describe('bicicleta.delete', () => {
    it('Elimina una bicicleta', (done) => {
      var aBici = Bicicleta.createInstance(1, "Verde", "Urbana", [37.886610834914016, -4.791306293646539]);
      var bBici = Bicicleta.createInstance(2, "Blanca", "Urbana", [37.885186766470405, -4.7914177352839795]);
      var cBici = Bicicleta.createInstance(3, "Verde", "Urbana", [37.877431464481596, -4.8012672656286846]);

      Bicicleta.add(aBici, function(err, newBici) {
        if (err) console.log(err);
        Bicicleta.add(bBici, function(err, newBici) {
          if (err) console.log(err);
          Bicicleta.add(cBici, function(err, newBici) {
            if (err) console.log(err);
            Bicicleta.allBicis(function(err, bicis) {
              console.log('Verificando que hayan 3 bicicletas.');
              expect(bicis.length).toBe(3);
            });

            Bicicleta.removeByCode(bBici.code, function(err) {
              if (err) console.log(err);
              Bicicleta.allBicis(function(err, bicis) {
                console.log('Verificando que hayan 2 bicicletas.');
                expect(bicis.length).toBe(2);
                done();
              }); 
            });
          });
        });
      });  
    });
  });
});
