var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res) {
  Bicicleta.allBicis(function(err, bicis) {
    if (err) console.log(err);
    res.status(200).json({
      bicicletas: bicis
    });  
  });
};

exports.bicicleta_read = function(req, res) {
  Bicicleta.findByCode(req.params.code, function(err, bici) {
    if (err) console.log(err);
    res.status(200).json({
      bicicleta: bici
    });
  });
};

exports.bicicleta_create = function(req, res) {
  var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
  Bicicleta.add(bici, function(err, newBici) { 
    if (err) console.log(err);
    res.status(200).json({
      bicicleta: newBici
    });
  });
};

exports.bicicleta_update = function(req, res) {
  Bicicleta.findByCode(req.body.code, function(err, bici) {
    if (err) console.log(err);
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    bici.save();
    res.status(200).json({
      bicicleta: bici
    });
  });
};

exports.bicicleta_delete = function(req, res) {
  Bicicleta.removeByCode(req.params.code, function(err) {
    if (err) console.log(err);
    res.status(204).send();  
  });
};
